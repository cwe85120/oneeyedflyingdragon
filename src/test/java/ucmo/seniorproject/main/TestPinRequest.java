package ucmo.seniorproject.main;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestPinRequest {
    @JsonProperty("account_id")
    private String accountId;
    @JsonProperty("account_type")
    private String accountType;
    @JsonProperty("requestor")
    private String user;

    public TestPinRequest(String accountId, String accountType, String user) {
        this.accountId = accountId;
        this.accountType = accountType;
        this.user = user;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
    @Override
    public String toString(){
        String str ="";
        try {
            str = new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return str;
    }
}
