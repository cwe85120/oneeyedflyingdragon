package ucmo.seniorproject.main;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestPinRedemptionRequest extends TestPinRequest {
    @JsonProperty("pin")
    private String pin;

    public TestPinRedemptionRequest(String accountId, String accountType, String user, String pin) {
        super(accountId, accountType, user);
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
    @Override
    public String toString(){
        String str ="";
        try {
            str = new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return str;
    }
}
