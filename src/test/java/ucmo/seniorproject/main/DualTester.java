package ucmo.seniorproject.main;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
public class DualTester {
    private TestPinRequest request1;
    private TestPinRedemptionRequest request2;
    private String accountId;
    private String accountType;
    private String user1;
    private String user2;

    public DualTester(String accountId, String accountType, String user1, String user2) {
        this.accountId = accountId;
        this.accountType = accountType;
        this.user1 = user1;
        this.user2 = user2;
        this.request1 = new TestPinRequest(this.accountId, this.accountType, this.user1);
    }
    private RequestBuilder buildRequest(String url, String content){
        return post("/pinme/"+url).accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE).content(content);
    }
    public void testPinRequest(MockMvc mockMvc) throws Exception {
        ResultActions actions = mockMvc.perform(buildRequest("requestpin",this.request1.toString())).andExpect(status().isOk())
                .andExpect(jsonPath("pin", is(notNullValue())));
        actions = actions.andDo(document("/pinme/requestpin",requestFields(fieldWithPath("account_id").description("The ID of the account the requested pin corrasponds with."),
                                                                                fieldWithPath("account_type").description("The type of the account the requested pin corrasponds with."),
                                                                                fieldWithPath("requestor").description("The username of the user requesting the pin.")),
                                                                responseFields(fieldWithPath("pin").description("The generated pin"))));
        MvcResult res = actions.andReturn();
        this.request2 = new TestPinRedemptionRequest(this.accountId, this.accountType, this.user2, getPinVal(res));
    }
    public void testPinRedeemptionRequest(MockMvc mockMvc) throws Exception {
        if(this.request2==null){return;}
        ResultActions actions = mockMvc.perform(buildRequest("redeempin",this.request2.toString())).andExpect(status().isOk())
                .andExpect(jsonPath("wasPinValid", is(notNullValue())))
                .andExpect(jsonPath("message", is(notNullValue())));
        actions = actions.andDo(document("/pinme/redeempin",requestFields(fieldWithPath("account_id").description("The ID of the account the pin being redeemed corrasponds with."),
                                                                                fieldWithPath("account_type").description("The type of the account the pin being redeemed corrasponds with."),
                                                                                fieldWithPath("requestor").description("The username of the user redeeming the pin."),
                                                                                fieldWithPath("pin").description("The pin being redeemed.")),
                                                                            responseFields(fieldWithPath("wasPinValid").description("Flag indicating if the pin was successfully redeemed."),
                                                                                            fieldWithPath("message").description("Text feedback for why redeemption failed or redeemption successful message. These responses are configureable via reedem.msg.* config options in the config.properties file."))));
        MvcResult res = actions.andReturn();
        this.request2 = new TestPinRedemptionRequest(this.accountId, this.accountType, this.user2, getPinVal(res));
    }
    private String getPinVal(MvcResult res) throws IOException {
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        map = mapper.readValue(res.getResponse().getContentAsString(), new TypeReference<Map<String, String>>(){});
        return map.get("pin");
    }
}
