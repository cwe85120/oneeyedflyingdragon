package ucmo.seniorproject.model.httpjson;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PinRequestResponse {
    @JsonProperty("pin")
    private String pin;
    public PinRequestResponse(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
