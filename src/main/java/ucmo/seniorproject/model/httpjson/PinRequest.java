package ucmo.seniorproject.model.httpjson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class PinRequest {
    @JsonProperty("account_id")
    private String accountId;
    @JsonProperty("account_type")
    private String accountType;
    private String ip;
    @JsonProperty("requestor")
    private String user;

    private Date requestTime;


    public PinRequest(){
        this.requestTime = new Date();
    }
    public PinRequest(String accountId, String accountType, String user, String ip) {
        this.accountId = accountId;
        this.accountType = accountType;
        this.user = user;
        this.ip = ip;
        this.requestTime = new Date();
    }
    public boolean isComplete(){
        boolean test1 = fieldFilled(accountType);
        boolean test2 = fieldFilled(accountId);
        boolean test3 = fieldFilled(user);
        return test1 && test3 && test2;
    }
    public static boolean fieldFilled(String field){
        boolean test = false;
        if(field != null){
            if(!field.equals("")){
                test = true;
            }
        }
        return test;
    }
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUser() { return user;}

    public void setUser(String user) { this.user = user;}

    public Date getRequestTime() {
        return new Date(requestTime.getTime());
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = new Date(requestTime.getTime());
    }


}
