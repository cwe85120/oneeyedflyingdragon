package ucmo.seniorproject.model.httpjson;

import com.fasterxml.jackson.annotation.JsonProperty;
import ucmo.seniorproject.config.PinRedemptionMsgConfig;
import ucmo.seniorproject.util.PinRedemptionResult;

public class PinRedemptionResponse {
    @JsonProperty("message")
    private String message;
    @JsonProperty("wasPinValid")
    private boolean wasPinValid;

    public PinRedemptionResponse(PinRedemptionResult result, PinRedemptionMsgConfig redeemConfig){
        this.wasPinValid = result.isValidRes();
        this.message = result.getMsg(redeemConfig);
    }

    public void set(PinRedemptionResult result, PinRedemptionMsgConfig redeemConfig){
        this.wasPinValid = result.isValidRes();
        this.message = result.getMsg(redeemConfig);
    }

    public String getMessage() {
        return message;
    }

    public boolean isWasPinValid() {
        return wasPinValid;
    }
}
