package ucmo.seniorproject.model.httpjson;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PinRedemptionRequest extends PinRequest {
    @JsonProperty("pin")
    private String pin;

    public PinRedemptionRequest(){
        super();
    }
    public String getPin() {
        return pin;
    }
    @Override
    public boolean isComplete(){
        return super.isComplete() && PinRequest.fieldFilled(pin);
    }
    public void setPin(String pin) {
        this.pin = pin;
    }
}
