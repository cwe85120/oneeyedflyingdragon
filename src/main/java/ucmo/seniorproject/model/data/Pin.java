package ucmo.seniorproject.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import ucmo.seniorproject.model.httpjson.PinRedemptionRequest;
import ucmo.seniorproject.model.httpjson.PinRequest;
import ucmo.seniorproject.util.DateParser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

public class Pin {
    private Integer id;
    private String pin;
    private String accountId;
    private String accountType;
    private String createIp;
    private String createUser;
    private String claimIp;
    private String claimUser;
    private Date requestTime;
    private Date expireTime;
    private Date claimTime;

    public Pin(String pin, String accountId, String accountType, String createIp, String createUser, Date requestTime, int daysToExpire) {
        this.pin = pin;
        this.accountId = accountId;
        this.accountType = accountType;
        this.createIp = createIp;
        this.createUser = createUser;
        this.requestTime = new Date(requestTime.getTime());
        this.expireTime = new Date(DateParser.calculateExperationTime(this.requestTime, daysToExpire).getTime());
    }
    public Pin(String pin, String accountId, String accountType, String createIp, String createUser, Date requestTime, Date expireTime) {
        this.pin = pin;
        this.accountId = accountId;
        this.accountType = accountType;
        this.createIp = createIp;
        this.createUser = createUser;
        this.requestTime = new Date(requestTime.getTime());
        this.expireTime = new Date(expireTime.getTime());
    }
    public Pin(PinRequest request, String pin, int daysToExpire ){
        this.pin = pin;
        this.accountId = request.getAccountId();
        this.accountType = request.getAccountType();
        this.createIp = request.getIp();
        this.createUser = request.getUser();
        this.requestTime = new Date(request.getRequestTime().getTime());
        this.expireTime = new Date(DateParser.calculateExperationTime(this.requestTime, daysToExpire).getTime());
    }
    public Pin(ResultSet rs) throws SQLException {
        String temp;
        this.id = rs.getInt("id");
        this.pin = rs.getString("pin");
        this.accountId = rs.getString("account_id");
        this.accountType = rs.getString("account_type");
        this.createIp = rs.getString("create_ip");
        this.createUser = rs.getString("create_user");

        try {
            temp = rs.getString("create_timestamp");
            this.requestTime = new Date(DateParser.textToDate(temp).getTime());
            temp = rs.getString("expire_timestamp");
            this.expireTime = new Date(DateParser.textToDate(temp).getTime());
            temp = rs.getString("claim_timestamp");
            if(!rs.wasNull()){
               this.claimTime =  new Date(DateParser.textToDate(temp).getTime());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        temp = rs.getString("claim_user");
        if(!rs.wasNull()){
            this.claimUser = temp;
        }
        temp = rs.getString("claim_ip");
        if(!rs.wasNull()){
            this.claimIp = temp;
        }

    }
    public String insert(){
        return "('"+this.accountType+"','"+this.accountId+"','"+this.pin+"','"+DateParser.dateToText(this.requestTime)+"','"+this.createIp+"','"+this.createUser+"','"+DateParser.dateToText(this.expireTime)+"')";
    }
    public String update(){
        return "claim_timestamp = '"+DateParser.dateToText(this.claimTime)+"', claim_user = '"+this.claimUser+"', claim_ip ='"+this.claimIp+"'";
    }
    public boolean claimed(){
        return !(claimIp == null && claimUser == null && claimTime == null);
    }
    public boolean expired(){
        Date date = new Date();
        int compare = this.expireTime.compareTo(date);
        return (compare <=0);
    }
    public void setClaimData(PinRedemptionRequest request){
        this.setClaimIp(request.getIp());
        this.setClaimTime(request.getRequestTime());
        this.setClaimUser(request.getUser());
    }
    public String getAccountType() {
        return accountType;
    }

    public Integer getId() {
        return id;
    }

    public String getPin() {
        return pin;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getCreateIp() {
        return createIp;
    }

    public String getCreateUser() {
        return createUser;
    }

    public String getClaimIp() {
        return claimIp;
    }

    public String getClaimUser() {
        return claimUser;
    }

    public Date getRequestTime() { return new Date(requestTime.getTime()); }

    public Date getExpireTime() {
        return new Date(expireTime.getTime());
    }

    public Date getClaimTime() {
        return new Date(claimTime.getTime());
    }

    public void setClaimIp(String claimIp) {
        this.claimIp = claimIp;
    }

    public void setClaimUser(String claimUser) {
        this.claimUser = claimUser;
    }

    public void setClaimTime(Date claimTime) { this.claimTime = new Date(claimTime.getTime()); }
}
