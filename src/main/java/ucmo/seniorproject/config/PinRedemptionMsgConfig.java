package ucmo.seniorproject.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ucmo.seniorproject.util.PinRedemptionResult;

@Component
@PropertySource("classpath:config.properties")
@ConfigurationProperties
public class PinRedemptionMsgConfig {
    @Value("${redeem.msg.valid}")
    private String valid;
    @Value("${redeem.msg.doesnotexist}")
    private String doesnotexist;
    @Value("${redeem.msg.expired}")
    private String expired;
    @Value("${redeem.msg.claimed}")
    private String claimed;
    @Value("${redeem.msg.invalid}")
    private String invalid;
    @Value("${redeem.msg.invalidRequest}")
    private String invalidRequest;
    @Value("${redeem.msg.error}")
    private String error;

    public String getMsg(PinRedemptionResult res){
        String msg ="";
        switch (res){
            case VALID:
                msg = this.valid;
                break;
            case CLAIMED:
                msg = claimed;
                break;
            case EXPIRED:
                msg = this.expired;
                break;
            case DOES_NOT_EXIST:
                msg = this.doesnotexist;
                break;
            case INVALID:
                msg = this.invalid;
                break;
            case ERROR:
                msg = this.error;
                break;
            case INVALID_REQUEST:
                msg = this.invalidRequest;
        }
        return msg;
    }
}
