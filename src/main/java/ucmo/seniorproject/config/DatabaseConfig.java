package ucmo.seniorproject.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ucmo.seniorproject.util.QueryBuilder;
import ucmo.seniorproject.util.SqlConnect;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

@Component
@PropertySource("classpath:config.properties")
@ConfigurationProperties
public class DatabaseConfig {
    @Value("${database.url}")
    private String url;
    @Value("${database.username}")
    private String username;
    @Value("${database.password}")
    private String password;
    @Value("${database.pinTable}")
    private String pinTable;

    public QueryBuilder getQueryBuilder(){
        return new QueryBuilder(this.pinTable);
    }
    public SqlConnect getConnection() throws ClassNotFoundException {
        return new SqlConnect(this.url, this.username, this.password);
    }
    public static void outputAllInResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int colCount = rsmd.getColumnCount();
        while(rs.next()){
            for (int i = 1; i <= colCount; i++){
                if (i > 1) System.out.print(", ");
                String columnValue = rs.getString(i);
                System.out.print(columnValue + " " + rsmd.getColumnName(i));
            }
            System.out.println();
        }
        rs.close();
    }

}
