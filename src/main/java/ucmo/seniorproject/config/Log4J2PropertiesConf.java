package ucmo.seniorproject.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4J2PropertiesConf {
	private static Logger logger = LogManager.getLogger();
	public void testLogger(){
		logger.debug("Debug Message");
		logger.info("Info Message");
		logger.warn("Warn Message");
		logger.error("Error Message");
		logger.fatal("Fatal Message");
	}
}
