package ucmo.seniorproject.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:config.properties")
@ConfigurationProperties
public class GeneralConfig {
    @Value("${pinme.experationTimeDays}")
    private String expirationTimeDays;

    public int getExpirationTimeDays() {
        return Integer.parseInt(expirationTimeDays);
    }

}
