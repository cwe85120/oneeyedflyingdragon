package ucmo.seniorproject.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ucmo.seniorproject.config.PinRedemptionMsgConfig;

public enum PinRedemptionResult {
    VALID(true, 0), DOES_NOT_EXIST(false,1), EXPIRED(false, 2), CLAIMED(false, 3), INVALID(false,4), ERROR(false,5), INVALID_REQUEST(false, 6);

    private boolean validRes;
    private int msgIndex;

    PinRedemptionResult(boolean validRes, int i){
        this.validRes = validRes;
        this.msgIndex = i;
    }

    public boolean isValidRes() {
        return validRes;
    }
    public String getMsg(PinRedemptionMsgConfig config){
        return config.getMsg(this);
    }
}
