package ucmo.seniorproject.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateParser {
    private static final String SQL_FORMAT = "yyyy-MM-dd kk:mm:ss.S";
    public static Date calculateExperationTime(Date start, int days){
        Calendar now = Calendar.getInstance();
        now.setTime(start);
        now.add(Calendar.DATE, days);
        return now.getTime();
    }
    public static String dateToText(Date d){
        return new SimpleDateFormat(SQL_FORMAT).format(d);
    }
    public static Date textToDate(String text) throws ParseException {
        return new SimpleDateFormat(SQL_FORMAT).parse(text);
    }
}
