package ucmo.seniorproject.util;

import java.sql.*;

public class SqlConnect {
    private Connection connection;
    private String url;
    private String user;
    private String password;

    public SqlConnect(String url, String user, String password)throws ClassNotFoundException{
        Class.forName("com.mysql.jdbc.Driver");
        this.url = url;
        this.user = user;
        this.password = password;
        this.connection = null;
    }

    public Connection getConnection() {
        return connection;
    }

    private void open() throws SQLException {
        this.connection = DriverManager.getConnection(url, user, password);
    }

    public void close() throws SQLException {
        connection.close();
    }

    public void insertStatement(String table, String pin) throws SQLException{
        open();
        Statement stmt;
        String query = "INSERT INTO " + table + "(account_type, account_id, pin, create_timestamp, create_ip, create_user, expire_timestamp) VALUES " + pin;
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException es){
            es.printStackTrace();
        }
    }

    public void updateStatement(String table, String update, String id) throws SQLException{
        open();
        Statement stmt;
        String query = "UPDATE " + table + " SET " + update + " WHERE id = " + id;
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException es){
            es.printStackTrace();
        }
    }

    public ResultSet getPinStatement(String table, String pin) throws SQLException{
        open();
        Statement stmt;
        String query = "SELECT * FROM " + table + " WHERE pin = " + pin;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
        } catch ( SQLException es ) {
            es.printStackTrace();

        }
        return rs;
    }

    public ResultSet getAllStatement(String table) throws SQLException{
        open();
        Statement stmt;
        String query = "SELECT * FROM " + table;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
        } catch (SQLException es){
            es.printStackTrace();
        }
        return rs;
    }

    ResultSet getCountStatement(String table, String pin) throws SQLException{
        open();
        Statement stmt;
        String query = "SELECT Count(*) FROM " + table + " WHERE pin = " + pin;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
        } catch (SQLException es){
            es.printStackTrace();
        }
        return rs;
    }

}
