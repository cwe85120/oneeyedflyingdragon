package ucmo.seniorproject.util;

import ucmo.seniorproject.config.DatabaseConfig;

import java.sql.*;
import java.util.Random;

public class PinFactory {
    private static final int PINLENGTH = 6;
    static String pin;
    public static final Random rand = new Random();

    public static String generatePin(DatabaseConfig config) throws ClassNotFoundException {

        SqlConnect connection;
        ResultSet resultSet;
        int count = 0;
        pin = "";

        do{
            while (pin.length() < PINLENGTH -1) {
                Integer randNum = rand.nextInt(10);
                pin += randNum.toString();
            }
            pin+=""+LuhnCheck.findCheckDigit(pin);
            try {
                connection = config.getConnection();
                resultSet = connection.getCountStatement(config.getQueryBuilder().table, pin);
                resultSet.next();
                count = resultSet.getInt(1);
                resultSet.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }while(count != 0);

        return pin;
    }

}
