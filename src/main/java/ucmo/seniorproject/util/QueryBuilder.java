package ucmo.seniorproject.util;

import ucmo.seniorproject.model.data.Pin;

import java.util.ArrayList;

public class QueryBuilder {
    public String table;
    public QueryBuilder(String table){
        this.table= table;
    }
    public  String countWherePinIs(String pin){
        return "SELECT Count(*) FROM "+this.table+" WHERE pin = '"+pin+"';";
    }
    public String getAll(){
        return "SELECT * FROM "+ this.table+";";
    }
    public String insertPins(ArrayList<Pin> pins){
        String q = "INSERT INTO "+this.table+ "(account_type, account_id, pin, create_timestamp, create_ip, create_user, expire_timestamp) VALUES ";
        int i = 0;
        StringBuilder sb = new StringBuilder();
        for(Pin p:pins){
            sb.append(p.insert());
            if(i < pins.size()-1){
                sb.append(", ");
            }
            i+=1;
        }
        q+=sb.toString();
        return q+";";
    }
    public String insertPin(Pin pin){
        String q = "INSERT INTO "+this.table+ "(account_type, account_id, pin, create_timestamp, create_ip, create_user, expire_timestamp) VALUES ";
        q+=pin.insert()+";";
        return q;
    }
    public String dropTable(){
        return "DROP TABLE IF EXISTS "+this.table;
    }
    public String createTable() {
        String create = "create table " + this.table + " (" +
                "id mediumint not null auto_increment," +
                "account_type varchar(256) not null," +
                "account_id varchar(256) not null," +
                "pin varchar(6) not null," +
                "create_timestamp timestamp not null," +
                "create_ip varchar(256) not null," +
                "create_user varchar(256) not null," +
                "expire_timestamp timestamp not null," +
                "claim_timestamp timestamp null," +
                "claim_user varchar(256) null," +
                "claim_ip varchar(256) null," +
                "primary key(id)" +
                ");";
            return create;
    }
    public String updatePin(Pin pin){
        String q="UPDATE "+this.table+" ";
        q+="SET "+pin.update();
        q+=" WHERE id = "+pin.getId()+";";
        return q;
    }
    public String getPin(String pin){
        String q = "SELECT * FROM "+this.table+" WHERE pin = '"+pin+"';";
        return q;
    }

}
