package ucmo.seniorproject.util;

public class LuhnCheck {
    public static int findCheckDigit(String number){
        return (doDigitSum(number+"0")*9)%10;
    }
    public static boolean mod10Check(String number){
        return (doDigitSum(number) %10) == 0;
    }
    private static int doDigitSum(String number){
        String[] digits = number.split("");
        int sum = 0;
        int temp = 0;
        boolean alt = false;
        for (int i = digits.length-1; i >=0; i--){
            temp = Integer.parseInt(digits[i]);
            if(alt){
                temp= temp*2;
                if(temp > 9){
                    temp-=9;
                }
            }
            sum+=temp;
            alt = !alt;
        }
        return sum;
    }
}
