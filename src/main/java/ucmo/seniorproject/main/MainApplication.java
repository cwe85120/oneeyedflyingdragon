package ucmo.seniorproject.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import ucmo.seniorproject.config.DatabaseConfig;
import ucmo.seniorproject.config.GeneralConfig;
import ucmo.seniorproject.config.PinRedemptionMsgConfig;

import java.sql.SQLException;

@SpringBootApplication
@EnableConfigurationProperties({PinRedemptionMsgConfig.class, DatabaseConfig.class, GeneralConfig.class})
public class MainApplication {

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}
}
