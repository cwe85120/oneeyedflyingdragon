package ucmo.seniorproject.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ucmo.seniorproject.config.DatabaseConfig;
import ucmo.seniorproject.config.GeneralConfig;
import ucmo.seniorproject.config.PinRedemptionMsgConfig;
import ucmo.seniorproject.model.data.Pin;
import ucmo.seniorproject.model.httpjson.PinRedemptionRequest;
import ucmo.seniorproject.model.httpjson.PinRedemptionResponse;
import ucmo.seniorproject.model.httpjson.PinRequest;
import ucmo.seniorproject.model.httpjson.PinRequestResponse;
import ucmo.seniorproject.util.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;

@RequestMapping("/pinme")
@RestController
public class MainController {
    private Logger logger;
    @Autowired
    private DatabaseConfig dbConfig;
    @Autowired
    private GeneralConfig generalConfig;
    @Autowired
    private PinRedemptionMsgConfig redeemConfig;
    @RequestMapping(value = "/requestpin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = {MediaType.APPLICATION_JSON_VALUE})
    public PinRequestResponse requestPin(@RequestBody(required=false) PinRequest request, HttpServletRequest http){
        logger = LogManager.getLogger();
        if(request.isComplete()){
            request.setIp(http.getRemoteAddr());
            logger.info("Incoming pin request received from: " + request.getIp());
            String pin ="";
            try {
                pin = PinFactory.generatePin(dbConfig);
                SqlConnect conn = dbConfig.getConnection();
                conn.insertStatement(dbConfig.getQueryBuilder().table, new Pin(request, pin, generalConfig.getExpirationTimeDays()).insert());
                DatabaseConfig.outputAllInResultSet(conn.getAllStatement(dbConfig.getQueryBuilder().table)); //can be commented out if dont want to see database after every request
                conn.close();
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return new PinRequestResponse(pin);
        }
        logger.warn("Incoming pin request received from: " + request.getIp() + " invalid! Did not have all fields filled out.");
        return new PinRequestResponse(this.redeemConfig.getMsg(PinRedemptionResult.INVALID_REQUEST));
    }
    @RequestMapping(value = "/redeempin", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = {MediaType.APPLICATION_JSON_VALUE})
    public PinRedemptionResponse redeemPin(@RequestBody(required=false) PinRedemptionRequest request, HttpServletRequest http){
        request.setIp(http.getRemoteAddr());
        logger = LogManager.getLogger();
        logger.info("Incoming pin redemption received from: " + request.getIp());
        Pin pin;
        PinRedemptionResult res = PinRedemptionResult.ERROR;
        if(!request.isComplete()){
            res = PinRedemptionResult.INVALID_REQUEST;
            logger.warn("Redemption request from " + request.getIp() + " invalid! Did not have all fields filled out.");
        }
        else if(!LuhnCheck.mod10Check(request.getPin())){
            res = PinRedemptionResult.INVALID;
            logger.warn("Redemption request from " + request.getIp() + " invalid! Did not pass Luhn Check!");
        }
        else{
            try {
                SqlConnect conn = dbConfig.getConnection();
                ResultSet rs = conn.getPinStatement(dbConfig.getQueryBuilder().table, request.getPin());
                if(rs.next()){
                    pin = new Pin(rs);
                    if(pin.claimed()){
                        res = PinRedemptionResult.CLAIMED;
                        logger.warn("Redemption request from " + request.getIp() + " already claimed!");
                    }
                    else if(pin.expired()){
                        res = PinRedemptionResult.EXPIRED;
                        logger.warn("Redemption request from " + request.getIp() + " has expired!");
                    }
                    else{
                        res = PinRedemptionResult.VALID;
                        logger.info("Redemption request from " + request.getIp() + " is valid!");
                        pin.setClaimData(request);
                        conn.updateStatement(dbConfig.getQueryBuilder().table, pin.update(), pin.getId().toString());
                        DatabaseConfig.outputAllInResultSet(conn.getAllStatement(dbConfig.getQueryBuilder().table)); //can be commented out if dont want to see database after every request
                    }
                }
                else{
                    res = PinRedemptionResult.DOES_NOT_EXIST;
                    logger.warn("Redemption request from " + request.getIp() + " invalid! Does not exist in the database!");
                }
                rs.close();
                conn.close();
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return new PinRedemptionResponse(res, redeemConfig);
    }
}
