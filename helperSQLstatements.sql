/*create the table from the assignment doc*/
create table pins (
id mediumint not null auto_increment,
account varchar(256) not null,
pin varchar(6) not null,
create_timestamp timestamp not null,
create_ip varchar(256) not null,
create_user varchar(256) not null,
expire_timestamp timestamp not null,
claim_timestamp timestamp null,
claim_user varchar(256) null,
claim_ip varchar(256) null,
primary key(id)                
);

/*Stuff the database with valid values. Maybe for testing?*/
insert into pins (account, pin, create_timestamp, create_ip, create_user,
                  expire_timestamp)
values ('100001','123456','2018-01-01 01:01:01','192.168.0.1','100000','2018-02-28 01:01:01'),
('100002','234567','2018-01-01 01:01:01','192.168.0.1','100000','2018-01-28 01:01:01'),
('100003','345678','2018-01-01 01:01:01','192.168.0.1','100000','2018-01-28 01:01:01'),
('100004','456789','2018-01-01 01:01:01','192.168.0.1','100000','2018-02-28 01:01:01'),
('100005','567891','2018-01-01 01:01:01','192.168.0.1','100000','2018-03-28 01:01:01');
 
 /*Get output of everything in the database*/
SELECT * FROM pins;